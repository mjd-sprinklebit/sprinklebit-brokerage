Pod::Spec.new do |s|
  s.name             = "SBBrokerage"
  s.version          = "0.0.1"
  s.summary          = "Payment - Subproject of the Sprinklebit."
  s.description      = <<-DESC
                       Here you will find Views, ViewControllers and Infractures classes for the Payment subproject.
                       DESC
  s.homepage         = "http://www.sprinklebit.com"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Jairo Junior" => "jairobjunior@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/sprinklebit/sprinklebit-brokerage.git", :tag => s.version.to_s }
  s.source_files     = "SBBrokerage/Classes", "SBBrokerage/Classes/**/*.{h,m}"
  s.resources        = "SBBrokerage/Resources/*", "SBBrokerage/**/*.xib"

  s.requires_arc     = true

  s.dependency 'SBCore'
  s.dependency 'SBVisualComponents'
  s.dependency 'HMSegmentedControl'

  s.prefix_header_contents = '#import <SBCore/SBCore.h>', '#import <SBVisualComponents/SBVisualComponents.h>'

  s.ios.deployment_target = '6.0'
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/SBBrokerage"' } 
end
