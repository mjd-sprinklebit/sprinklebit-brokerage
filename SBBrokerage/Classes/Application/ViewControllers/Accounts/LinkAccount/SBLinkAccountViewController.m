//
//  SBLinkAccountViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 31/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBLinkAccountViewController.h"
#import "SBAlertListViewController.h"
#import "SBBrokerageDataSource.h"
#import "SBUITextField.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface SBLinkAccountViewController ()

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet SBUITextField *linkNicknameTextField;
@property (weak, nonatomic) IBOutlet SBUITextField *bankRoutingNumberTextView;
@property (weak, nonatomic) IBOutlet SBUITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet SBUITextField *bankAccountNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *brokerageAccountSelector;
@property (weak, nonatomic) IBOutlet SBUITextField *nameOnAccountTextField;
@property (weak, nonatomic) IBOutlet UIButton *accountTypeSelector;
@property (weak, nonatomic) IBOutlet SBUIButton *linkNewAccountButton;

@end

@implementation SBLinkAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Link New Account"];
    
    [_bankRoutingNumberTextView addPlaceholderWithIconLabel:[self getTextViewLabelWithPlaceholder:@"#"]];
    [_bankAccountNumberTextField addPlaceholderWithIconLabel:[self getTextViewLabelWithPlaceholder:@"#"]];
    
    [_linkNewAccountButton setAppearanceLightTheme];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)brokerageAccountSelectorTapped:(id)sender {
    NSArray *listItems = [[[SBBrokerageDataSource getBrokerageAccountList] allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    SBAlertListViewController *alertList = [[SBAlertListViewController alloc] initWithTitle:@"Select Broekerage Account" listItems:listItems];
    [alertList onSelectedListener:^(id object, NSInteger index) {
        [alertList dismissWithCompletion:^{
            [_brokerageAccountSelector setTitle:object forState:UIControlStateNormal];
        }];
    }];
    [alertList show];
    
}

- (IBAction)accountTypeSelectorTapped:(id)sender {
    NSArray *listItems = [[[SBBrokerageDataSource getAccountTypeList] allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    SBAlertListViewController *alertList = [[SBAlertListViewController alloc] initWithTitle:@"Select Account Type" listItems:listItems];
    [alertList onSelectedListener:^(id object, NSInteger index) {
        [alertList dismissWithCompletion:^{
            [_accountTypeSelector setTitle:object forState:UIControlStateNormal];
        }];
    }];
    [alertList show];
    
}

- (UILabel *) getTextViewLabelWithPlaceholder: (NSString *) placeholder {
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 15, 20, 20)];
    label.text = placeholder;
    label.numberOfLines = 1;
    label.adjustsFontSizeToFitWidth = YES;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor darkGrayColor];
    
    return label;
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 688)];
}
@end
