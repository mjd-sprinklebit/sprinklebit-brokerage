//
//  SBNewAccountFinalStepViewController.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 29/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPPZViewInstantiator.h"

@interface SBNewAccountFinalStepViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
