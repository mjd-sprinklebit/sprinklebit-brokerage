//
//  SBNewAccountFinalStepViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 29/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBNewAccountFinalStepViewController.h"
#import "SBCheckBoxView.h"
#import "SBUIButton.h"
#import "SBBrokerageDataSource.h"
#import "SBFinalStepTableHeader.h"
#import "SBFinalStepTableFooter.h"
#import "SBFinalStepTableViewCell.h"
#import "UIView+SBExtension.h"

@interface SBNewAccountFinalStepViewController () <SBFinalStepTableFooterDelegate>
{
    NSArray *docsArray;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet SBUIButton *registerAccountButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SBCheckBoxView *acceptCheckboxView;
@property (weak, nonatomic) IBOutlet UILabel *checkboxLabel;

@end

@implementation SBNewAccountFinalStepViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"New Account";
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView registerClass:[SBFinalStepTableViewCell class] forCellReuseIdentifier:NSStringFromClass([SBFinalStepTableViewCell class])];
    
    self.tableView.showsVerticalScrollIndicator = YES;
    [self.tableView flashScrollIndicators];
    
    docsArray = [SBBrokerageDataSource getDocsList];
    
    SBFinalStepTableHeader *headerView = [SBFinalStepTableHeader loadFromNib];
    SBFinalStepTableFooter *footerView = [SBFinalStepTableFooter loadFromNib];
    [footerView setDelegate:self];
    
    self.tableView.tableFooterView = footerView;
    self.tableView.tableHeaderView = headerView;
    
    [self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.tableView.tableFooterView setOriginY:504-126];
}

#pragma mark - TableView
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [docsArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SBFinalStepTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SBFinalStepTableViewCell class])];
    
    // TODO: remove that in futuro, this is here just to see some pdf off icons on list
    // The image that will be used depends off system's bussiness rules
    if (indexPath.row >=3 && indexPath.row <= 7) {
        cell.imageView.image = [UIImage imageNamed:@"ico-pdf-on"];
    } else {
        cell.imageView.image = [UIImage imageNamed:@"ico-pdf-off"];
    }
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:10.0f]];
    [cell.textLabel setTextColor:[UIColor colorWithRed:75/255.0f green:120/255.0f blue:0/255.0f alpha:1.0f]];
    
    cell.textLabel.text = docsArray[indexPath.row];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

#pragma mark - SBFinalStepTableFooterDelegate

- (void)didRegisterAccountButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
