//
//  SBViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 27/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBNewAccountFirstStepViewController.h"
#import "SBUITextField.h"
#import "SBUIButton.h"
#import "SBNewAccountSecondStepViewController.h"
#import "TPKeyboardAvoidingCollectionView.h"
#import "SBAlertListViewController.h"
#import "SBBrokerageDataSource.h"

@interface SBNewAccountFirstStepViewController ()

@property (weak, nonatomic) IBOutlet SBUIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *accountTypeSelectorButton;
@property (weak, nonatomic) IBOutlet UIButton *accountingTradeTypeSelectorButton;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingCollectionView *scrollView;

@property (nonatomic, assign) UITextField* activeField;

@end

@implementation SBNewAccountFirstStepViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"New Account"];
    
    [_startButton setAppearanceLightTheme];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.scrollView setContentSize:CGSizeMake(320, 504)];
}

#pragma mark - TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)startButtonClicked:(id)sender {
    SBNewAccountSecondStepViewController *newAccountSecondViewController = [SBNewAccountSecondStepViewController new];
    [self.navigationController pushViewController:newAccountSecondViewController animated:YES];
}

- (IBAction)accountTypeButtonTapped:(id)sender {
  [self showAlertModalWithDict:[SBBrokerageDataSource getAccountTypeList] andTitle:@"Select Account Type" fromButton:_accountTypeSelectorButton];
}

- (IBAction)accountTradeTypeButtonTapped:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getAccountTradeTypeList] andTitle:@"Select Account Trade Type" fromButton:_accountingTradeTypeSelectorButton];
}

- (void)showAlertModalWithDict:(NSDictionary *)dictionary andTitle:(NSString *)title fromButton:(UIButton *)button
{
    NSArray *listItems = [[dictionary allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    SBAlertListViewController *alertList = [[SBAlertListViewController alloc] initWithTitle:title listItems:listItems];
    [alertList onSelectedListener:^(id object, NSInteger index) {
        [alertList dismissWithCompletion:^{
            [button setTitle:object forState:UIControlStateNormal];
        }];
    }];
    [alertList show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
