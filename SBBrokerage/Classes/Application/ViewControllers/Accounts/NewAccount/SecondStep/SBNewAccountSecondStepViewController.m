//
//  SBNewAccountSecondStepViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 29/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBNewAccountSecondStepViewController.h"
#import "SBCheckBoxView.h"
#import "SBUIButton.h"
#import "SBNewAccountFinalStepViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SBAlertListViewController.h"
#import "SBBrokerageDataSource.h"

@interface SBNewAccountSecondStepViewController ()

@property (weak, nonatomic) IBOutlet SBCheckBoxView *mmfSweepCheckbox;
@property (weak, nonatomic) IBOutlet SBUIButton *continueButton;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *cashStandingInstructionSelector;
@property (weak, nonatomic) IBOutlet UIButton *dividendStandInstructions;
@property (weak, nonatomic) IBOutlet UIButton *dividendReinvestmentInstructions;

- (IBAction)continueButtonClicked:(id)sender;

@end

@implementation SBNewAccountSecondStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"New Account";
    
    [_mmfSweepCheckbox setChecked:NO];
    [_mmfSweepCheckbox setText:@"MMF Sweep"];
    [_mmfSweepCheckbox setAppearanceLightTheme];
    
    [_continueButton setAppearanceLightTheme];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButtonClicked:(id)sender {
    SBNewAccountFinalStepViewController *finalStepViewController = [SBNewAccountFinalStepViewController new];
    [self.navigationController pushViewController:finalStepViewController animated:YES];
}

- (IBAction)cashStandingInstructionsTapped:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getCashStandingInstructionsList] andTitle:@"Select Cash Standing" fromButton:_cashStandingInstructionSelector];
}

- (IBAction)dividendStandingInstructionsTapped:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getDividendStandingInstructionsList] andTitle:@"Select Dividend Standing" fromButton:_dividendStandInstructions];
}

- (IBAction)dividendReinsvestmentInstructionsTapped:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getDividendReinsvestmentInstructionsList] andTitle:@"Select Dividend Reinsvestment" fromButton:_dividendReinvestmentInstructions];
}

- (void) showAlertModalWithDict: (NSDictionary *) dictionary andTitle:(NSString *) title fromButton:(UIButton *) button {
    NSArray *listItems = [[dictionary allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    SBAlertListViewController *alertList = [[SBAlertListViewController alloc] initWithTitle:title listItems:listItems];
    [alertList onSelectedListener:^(id object, NSInteger index) {
        [alertList dismissWithCompletion:^{
            [button setTitle:object forState:UIControlStateNormal];
        }];
    }];
    [alertList show];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 504)];
}

@end
