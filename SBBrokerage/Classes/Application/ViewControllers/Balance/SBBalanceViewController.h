//
//  SBBalanceViewController.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 05/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPPZViewInstantiator.h"

@interface SBBalanceViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
