//
//  SBBalanceViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 05/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBBalanceViewController.h"
#import "SBBrokerageDataSource.h"
#import "SBBalanceTableViewCell.h"
#import "SBBalanceTableViewSession.h"
#import "SBStockVariationLabel.h"
#import "SBBalanceItem.h"
#import "SBBalance.h"

@interface SBBalanceViewController () {
    SBBalance *balance;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SBStockVariationLabel *stockVariation;

@end

@implementation SBBalanceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Balance";
    
    balance = [SBBrokerageDataSource getBalanceData];
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SBBalanceTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SBBalanceTableViewCell class])];
    
    [_stockVariation setPercentageValue:8.07];
    [_stockVariation variationWentUp];
    [_stockVariation setIndicatorPosition:SBStockVariationIndicatorPositionRight];
}

#pragma mark - TableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [balance.items count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SBBalanceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SBBalanceTableViewCell class])];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"$###,###,###.##"];
    
    SBBalanceItem *balanceItem = (SBBalanceItem *) [balance.items objectAtIndex:indexPath.row];
    cell.titleLabel.text = balanceItem.title;
    cell.startOfDayLabel.text = [numberFormatter stringFromNumber:balanceItem.startOfDayAmount];
    cell.realTimeLabel.text = [numberFormatter stringFromNumber:balanceItem.realTimeAmount];
    cell.diffLabel.text = [numberFormatter stringFromNumber:balanceItem.diffAmount];
    
    // set background color
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [SBBalanceTableViewSession loadFromNib];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 26.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
