//
//  SBBrokerageTableViewController.m
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import "SBBrokerageFeesTableViewController.h"
#import "SBBrokerageFeesTableViewCell.h"
#import "SBBrokerageFeesHeaderView.h"
#import "SBBrokerageFeesSectionView.h"
#import "SBBrokerageDatasource.h"

@interface SBBrokerageFeesTableViewController ()

@property NSArray *items;

@end

@implementation SBBrokerageFeesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Brokerage Fees"];
    
    self.items = [SBBrokerageDataSource brokerageFees];
    
    [self setupTableView];
}

- (void)setupTableView
{
    SBBrokerageFeesHeaderView *headerView = [SBBrokerageFeesHeaderView loadFromNib];
    headerView.items = self.items;
    self.tableView.tableHeaderView = headerView;
    self.tableView.backgroundColor = [UIColor colorWithRed:0.929 green:0.929 blue:0.929 alpha:1];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SBBrokerageFeesTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SBBrokerageFeesTableViewCell class])];
}

#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [SBBrokerageFeesSectionView loadFromNib];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SBBrokerageFeesTableViewCell *cell = (SBBrokerageFeesTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SBBrokerageFeesTableViewCell class]) forIndexPath:indexPath];
    
    SBBrokerageFee *fee = self.items[indexPath.row];
    [cell setBrokerageFee:fee];
    
    return cell;
}


@end
