//
//  SBMainBrokerageViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 29/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBMainBrokerageViewController.h"
#import "SBNewAccountFirstStepViewController.h"
#import "SBLinkAccountViewController.h"
#import "SBBalanceViewController.h"
#import "SBTransactionsViewController.h"
#import "SBBrokerageFeesTableViewController.h"

@interface SBMainBrokerageViewController ()

@end

@implementation SBMainBrokerageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
//    SBBalanceViewController *viewController = [SBBalanceViewController new];
//    SBNewAccountFirstStepViewController *viewController = [[SBNewAccountFirstStepViewController alloc] init];
//    SBLinkAccountViewController *viewController = [SBLinkAccountViewController new];
    UIViewController *viewController = [SBBrokerageFeesTableViewController new];
    [self openModalUsingBackgroundColorWithViewController:viewController];
}

@end
