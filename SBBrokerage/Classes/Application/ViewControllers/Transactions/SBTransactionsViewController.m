//
//  SBTransactionsViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBTransactionsViewController.h"
#import "SBUIButton.h"
#import "SBBrokerageDataSource.h"
#import "SBTransactionsTableViewCell.h"
#import "SBTransationTabControl.h"
#import "SBTransactionsTableHeader.h"
#import "SBTransaction.h"

@interface SBTransactionsViewController () {
    NSArray *transactions;
}

@property (weak, nonatomic) IBOutlet SBUIButton *withdrawButton;
@property (weak, nonatomic) IBOutlet SBUIButton *depositButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet SBStockVariationLabel *stockVariation;
@property (weak, nonatomic) IBOutlet SBLiveTradeLabel *liveWithdraw;
@property (weak, nonatomic) IBOutlet SBLiveTradeLabel *liveDeposit;

@end

@implementation SBTransactionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Transactions"];
    transactions = [SBBrokerageDataSource getTransactionsData];
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([SBTransactionsTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SBTransactionsTableViewCell class])];
    
    [_withdrawButton setAppearanceDarkTheme];
    [_depositButton setAppearanceLightTheme];
    
    [_stockVariation setPercentageValue:8.07];
    [_stockVariation variationWentUp];
    [_stockVariation setIndicatorPosition:SBStockVariationIndicatorPositionRight];
    
    [self.liveDeposit changeStatusToLive];
    [self.liveWithdraw changeStatusToLive];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [transactions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SBTransactionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SBTransactionsTableViewCell class])];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"$###,###,###.##"];
    
    SBTransaction *transaction = (SBTransaction *) [transactions objectAtIndex:indexPath.row];
    cell.dateLabel.text = [formatter stringFromDate:transaction.date];
    cell.accountLabel.text = [NSString stringWithFormat:@"%@\n%@", transaction.accountName, transaction.accountNumber];
    cell.amountLabel.text = [numberFormatter stringFromNumber:transaction.amount];
    
    // TODO: remove this, is just a simulation
    if (indexPath.row > 2 && indexPath.row <=4) {
        cell.status = TransactionStatusNegative;
    }
    
    // set background color
    if (indexPath.row % 2 == 1) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,26)];
    SBTransactionsTableHeader *tableHeader = [SBTransactionsTableHeader loadFromNib];
    
    [header addSubview:tableHeader];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 26.0f;
}

- (IBAction)depositButtonTapped:(id)sender
{
    SBTransationTabControl *viewController = [SBTransationTabControl new];
    [viewController setTransferType:SBTransactionTransferTypeDeposit];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)withDrawButtonTapped:(id)sender
{
    SBTransationTabControl *viewController = [SBTransationTabControl new];
    [viewController setTransferType:SBTransactionTransferTypeWithDraw];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
