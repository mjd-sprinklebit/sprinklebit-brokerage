//
//  SBOnlineTransferACHViewController.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTransationTabControl.h"

@interface SBOnlineTransferACHViewController : UIViewController

@property (nonatomic) SBTransactionTransferType transferType;

@end
