//
//  SBOnlineTransferACHViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBOnlineTransferACHViewController.h"
#import "SBUIButton.h"
#import "SBUITextField.h"
#import "SBBrokerageDataSource.h"
#import "SBAlertListViewController.h"

@interface SBOnlineTransferACHViewController ()

@property (weak, nonatomic) IBOutlet SBUIButton *withDrawButton;
@property (weak, nonatomic) IBOutlet UIButton *toAccountSelector;
@property (weak, nonatomic) IBOutlet UIButton *fromAccountSelector;
@property (weak, nonatomic) IBOutlet SBUITextField *dollarAmountTextField;
@property (weak, nonatomic) IBOutlet UIImageView *transferImage;
@property (weak, nonatomic) IBOutlet UIImageView *dollarImage;
@property (weak, nonatomic) IBOutlet UIImageView *fromImage;
@property (weak, nonatomic) IBOutlet UIImageView *toImage;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@end

@implementation SBOnlineTransferACHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_withDrawButton setAppearanceLightTheme];
    [self configView];
}

- (void) configView {
    
    self.arrowImage.image = [UIImage imageNamed:@"ico-arrow-right-blue"];
    
    if (self.transferType == SBTransactionTransferTypeDeposit) {
        self.transferImage.image = [UIImage imageNamed:@"img-ach-deposit"];
        self.dollarImage.image = [UIImage imageNamed:@"ico-dollar-coin-green"];
        self.fromImage.image = [UIImage imageNamed:@"ico-safe"];
        self.toImage.image = [UIImage imageNamed:@"ico-spriklebit-gray"];
        [self.withDrawButton setTitle:@"Deposit" forState:UIControlStateNormal];
        [self.withDrawButton setImage:[UIImage imageNamed:@"ico-deposit"] forState:UIControlStateNormal];
    } else {
        self.transferImage.image = [UIImage imageNamed:@"img-ach-withdrawt"];
        self.dollarImage.image = [UIImage imageNamed:@"ico-dollar-coin-red"];
        self.fromImage.image = [UIImage imageNamed:@"ico-spriklebit-gray"];
        self.toImage.image = [UIImage imageNamed:@"ico-safe"];

        [self.withDrawButton setTitle:@"Withdraw" forState:UIControlStateNormal];
        [self.withDrawButton setImage:[UIImage imageNamed:@"ico-bt-withdraw-white"] forState:UIControlStateNormal];
    }
    
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 15, 20, 20)];
    fromLabel.text = @"$";
    fromLabel.numberOfLines = 1;
    fromLabel.adjustsFontSizeToFitWidth = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor darkGrayColor];
    
    [_dollarAmountTextField addPlaceholderWithIconLabel:fromLabel];
}
- (IBAction)fromAccountButtonClicked:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getBrokerageAccountList] andTitle:@"Select Account" fromButton:_fromAccountSelector];
}
- (IBAction)toAccountButtonClicked:(id)sender {
    [self showAlertModalWithDict:[SBBrokerageDataSource getBrokerageAccountList] andTitle:@"Select Account" fromButton:_toAccountSelector];
}

- (void)showAlertModalWithDict:(NSDictionary *)dictionary andTitle:(NSString *)title fromButton:(UIButton *)button
{
    NSArray *listItems = [[dictionary allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    SBAlertListViewController *alertList = [[SBAlertListViewController alloc] initWithTitle:title listItems:listItems];
    [alertList onSelectedListener:^(id object, NSInteger index) {
        [alertList dismissWithCompletion:^{
            [button setTitle:object forState:UIControlStateNormal];
        }];
    }];
    [alertList show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
