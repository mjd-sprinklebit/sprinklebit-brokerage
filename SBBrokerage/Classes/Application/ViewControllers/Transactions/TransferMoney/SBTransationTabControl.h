//
//  SBTransationTabControl.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBTransationTabControl : UIViewController

typedef enum {
    SBTransactionTransferTypeDeposit,
    SBTransactionTransferTypeWithDraw
} SBTransactionTransferType;

@property (nonatomic) SBTransactionTransferType transferType;

@end
