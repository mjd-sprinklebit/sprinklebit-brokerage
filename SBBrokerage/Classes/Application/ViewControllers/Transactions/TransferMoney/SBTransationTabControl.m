//
//  SBTransationTabControl.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBTransationTabControl.h"
#import "SBUITabControl.h"
#import "SBOnlineTransferACHViewController.h"
#import "SBWireTransferViewController.h"
#import "SBCheckTransferViewController.h"
#import "SBTabControlViewController.h"

@interface SBTransationTabControl ()

@end

@implementation SBTransationTabControl

- (void)viewDidLoad
{
    [super viewDidLoad];

    // setting background
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-brokerage-transfermoney"]];
    bgImageView.frame = self.view.bounds;
    [self.view addSubview:bgImageView];
    [self.view sendSubviewToBack:bgImageView];
    
    [self addTabControl];
}

- (void)addTabControl
{
    SBUITabControl *tabControl = [[SBUITabControl alloc] initWithFrame:CGRectMake(0, 0, 320, 38)];
    [tabControl setSectionTitles:@[@"Online Transfer ACH", @"Wire Transfer", @"Check Transfer"]];
    [tabControl setAppearanceLightTheme];
    
    SBOnlineTransferACHViewController *tab1ViewController = [[SBOnlineTransferACHViewController alloc] init];
    [tab1ViewController setTransferType:self.transferType];
    
    SBWireTransferViewController *tab2ViewController = [[SBWireTransferViewController alloc] init];
    SBCheckTransferViewController *tab3ViewController = [[SBCheckTransferViewController alloc] init];
    
    SBTabControlViewController *tabControlViewController = [[SBTabControlViewController alloc] initWithTabControl:tabControl viewControllers:@[tab1ViewController, tab2ViewController, tab3ViewController]];
    [self.view addSubview:tabControlViewController.view];
    [self addChildViewController:tabControlViewController];
    
    if (self.transferType == SBTransactionTransferTypeDeposit) {
        self.title = @"Deposit Money";
    } else {
        self.title = @"Withdraw Money";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
