//
//  SBWireTransferViewController.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBWireTransferViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface SBWireTransferViewController ()

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end

@implementation SBWireTransferViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}


- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 472)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
