//
//  SBFinalStepTableFooter.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 14/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBCheckBoxView.h"

@protocol SBFinalStepTableFooterDelegate <NSObject>

- (void)didRegisterAccountButtonTap;

@end

@interface SBFinalStepTableFooter : UIView

@property (nonatomic, assign) id<SBFinalStepTableFooterDelegate> delegate;

@property (strong, nonatomic) IBOutlet SBUIButton *registerAccountButton;
@property (strong, nonatomic) IBOutlet SBCheckBoxView *checkbox;
@property (strong, nonatomic) IBOutlet UILabel *checkboxLabel;

@end
