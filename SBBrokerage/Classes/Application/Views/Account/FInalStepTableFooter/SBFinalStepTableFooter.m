//
//  SBFinalStepTableFooter.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 14/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBFinalStepTableFooter.h"

@implementation SBFinalStepTableFooter

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setupView];
}

- (void)setupView
{
    [self.registerAccountButton setAppearanceLightTheme];
    [self.checkbox setAppearanceLightTheme];
    [self.checkbox setChecked:!self.checkbox.checked];
    
    UITapGestureRecognizer *tapCheckboxLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkboxLabelTapped)];
    [self.checkboxLabel addGestureRecognizer:tapCheckboxLabel];
    
    [_registerAccountButton addTarget:self action:@selector(didRegisterAccountPress:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)checkboxLabelTapped
{
    [self.checkbox setChecked:!self.checkbox.checked];
}

- (void)didRegisterAccountPress:(id)sender
{
    if (self.delegate) {
        [self.delegate didRegisterAccountButtonTap];
    }
}

@end
