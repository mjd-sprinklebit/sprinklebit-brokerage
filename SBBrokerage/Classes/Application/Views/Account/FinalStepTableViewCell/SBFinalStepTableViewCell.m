//
//  SBFinalStepTableViewCell.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 17/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBFinalStepTableViewCell.h"

@implementation SBFinalStepTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGRect tmpFrame = self.imageView.frame;
    tmpFrame.origin.x += 25;
    self.imageView.frame = tmpFrame;
    
    tmpFrame = self.textLabel.frame;
    tmpFrame.origin.x += 25;
    self.textLabel.frame = tmpFrame;
    
    tmpFrame = self.detailTextLabel.frame;
    tmpFrame.origin.x += 25;
    self.detailTextLabel.frame = tmpFrame;
}

@end
