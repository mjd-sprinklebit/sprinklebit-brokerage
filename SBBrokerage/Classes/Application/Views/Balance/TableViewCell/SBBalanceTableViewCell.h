//
//  SBBalanceTableViewCell.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 06/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBBalanceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *startOfDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *realTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *diffLabel;

@end
