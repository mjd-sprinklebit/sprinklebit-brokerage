//
//  SBBalanceTableViewCell.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 06/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBBalanceTableViewCell.h"

@implementation SBBalanceTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
