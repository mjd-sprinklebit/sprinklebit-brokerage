//
//  SBBalanceTableViewSession.m
//  SBBrokerage
//
//  Created by Jairo Junior on 8/8/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBBalanceTableViewSession.h"

@implementation SBBalanceTableViewSession

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
