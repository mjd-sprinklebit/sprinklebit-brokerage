//
//  SBBrokerageHeaderCell.h
//  SBAlert
//
//  Created by Klevison Matias on 8/22/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBBrokerageFee.h"
#import "KAProgressLabel.h"

@interface SBBrokerageFeesHeaderCell : UICollectionViewCell

@property (nonatomic) SBBrokerageFee *brokerageFee;

@property (weak, nonatomic) IBOutlet KAProgressLabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icoImageView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
