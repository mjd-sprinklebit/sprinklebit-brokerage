//
//  SBBrokerageHeaderCell.m
//  SBAlert
//
//  Created by Klevison Matias on 8/22/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import "SBBrokerageFeesHeaderCell.h"

@implementation SBBrokerageFeesHeaderCell

- (void)setBrokerageFee:(SBBrokerageFee *)brokerageFee
{
    _brokerageFee = brokerageFee;
    
    [super awakeFromNib];
    UIColor *c1 = [UIColor colorWithRed:0.831 green:0.831 blue:0.831 alpha:1];
    [self.progressLabel setColorTable:@{
                                        NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor) : c1,
                                        NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor) : brokerageFee.color
                                        }
     ];
    
    [_progressLabel setProgress:(float)brokerageFee.percent/100];
    self.icoImageView.image = brokerageFee.ico;
    self.descLabel.text = brokerageFee.desc;
    self.percentLabel.text = [NSString stringWithFormat:@"%G%%",brokerageFee.percent];
}

@end
