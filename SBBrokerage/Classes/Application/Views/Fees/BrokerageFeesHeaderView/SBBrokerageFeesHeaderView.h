//
//  SBBrokerageHeaderView.h
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPPZViewInstantiator.h"

@interface SBBrokerageFeesHeaderView : UIView <UICollectionViewDataSource,UICollectionViewDelegate>

@property NSArray *items;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
