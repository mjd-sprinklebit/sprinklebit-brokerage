//
//  SBBrokerageHeaderView.m
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import "SBBrokerageFeesHeaderView.h"
#import "SBBrokerageFeesHeaderCell.h"
#import "SBBrokerageFee.h"

@implementation SBBrokerageFeesHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupCollectionView];
}

- (void)setupCollectionView
{
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([SBBrokerageFeesHeaderCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([SBBrokerageFeesHeaderCell class])];
    [self.collectionView setAutoresizesSubviews:NO];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SBBrokerageFeesHeaderCell *cell = (SBBrokerageFeesHeaderCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SBBrokerageFeesHeaderCell class]) forIndexPath:indexPath];
    
    SBBrokerageFee *fee = self.items[indexPath.row];
    [cell setBrokerageFee:fee];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(94, 165);
}

@end
