//
//  SBBrokerageSectionView.m
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import "SBBrokerageFeesSectionView.h"

@implementation SBBrokerageFeesSectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
