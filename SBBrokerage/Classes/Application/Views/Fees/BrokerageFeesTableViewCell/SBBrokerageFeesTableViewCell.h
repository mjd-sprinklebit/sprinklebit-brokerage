//
//  SBBrokerageTableViewCell.h
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBBrokerageFee.h"

@interface SBBrokerageFeesTableViewCell : UITableViewCell

@property (nonatomic) SBBrokerageFee *brokerageFee;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

@end
