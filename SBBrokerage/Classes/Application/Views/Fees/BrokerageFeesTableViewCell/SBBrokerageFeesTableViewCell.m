//
//  SBBrokerageTableViewCell.m
//  SBAlert
//
//  Created by Klevison Matias on 8/21/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import "SBBrokerageFeesTableViewCell.h"
#import "NSString+SBFormatted.h"

@implementation SBBrokerageFeesTableViewCell

- (void)setBrokerageFee:(SBBrokerageFee *)brokerageFee
{
    _brokerageFee = brokerageFee;
    self.percentLabel.text = [NSString stringWithFormat:@"%G%%",brokerageFee.percent];
    self.valueLabel.text = [NSString currencyFormatterWithDouble:brokerageFee.feeValue withMaximumFractionDigits:2];
    self.nameLabel.text = brokerageFee.name;
}

@end
