//
//  SBTransactionsTableViewCell.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBTransactionsTableViewCell : UITableViewCell

enum {
    TransactionStatusPositive,
    TransactionStatusNegative
};

typedef NSInteger TransactionStatus;

@property (weak, nonatomic) IBOutlet UIImageView *sideImage;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@property (nonatomic) TransactionStatus status;

@end
