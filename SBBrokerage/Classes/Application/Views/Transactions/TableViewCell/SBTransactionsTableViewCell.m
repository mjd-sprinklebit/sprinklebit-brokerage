//
//  SBTransactionsTableViewCell.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 07/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBTransactionsTableViewCell.h"

@implementation SBTransactionsTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) setStatus:(TransactionStatus)status {
    _status = status;
    
    if (status == TransactionStatusPositive) {
        [self.sideImage setImage:[UIImage imageNamed:@"ico-arrow-right-green"]];
        self.amountLabel.textColor = [UIColor blackColor];
    } else {
        [self.sideImage setImage:[UIImage imageNamed:@"ico-arrow-left-red"]];
        self.amountLabel.textColor = [UIColor colorWithRed:213.0/255.0 green:41.0/255.0 blue:41.0/255.0 alpha:1.0];
    }
}
@end
