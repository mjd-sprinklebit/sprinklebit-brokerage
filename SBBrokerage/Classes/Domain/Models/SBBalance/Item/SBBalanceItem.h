//
//  SBBalance.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 13/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBBalanceItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *startOfDayAmount;
@property (nonatomic, strong) NSNumber *realTimeAmount;
@property (nonatomic, strong) NSNumber *diffAmount;

@end
