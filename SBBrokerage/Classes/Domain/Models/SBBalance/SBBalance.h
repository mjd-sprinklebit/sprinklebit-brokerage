//
//  SBBalance.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 13/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBBalance : NSObject

@property (nonatomic, strong) NSArray *items;

@end
