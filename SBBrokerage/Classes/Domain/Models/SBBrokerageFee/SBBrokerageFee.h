//
//  SBBrokerageFee.h
//  SBAlert
//
//  Created by Klevison Matias on 8/22/14.
//  Copyright (c) 2014 MJD Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBBrokerageFee : NSObject

@property UIImage *ico;
@property NSString *name;
@property NSString *desc;
@property double feeValue;
@property double brokerageValue;
@property double percent;
@property UIColor *color;

@end
