//
//  SBCashStandingInstruction.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 13/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBCashStandingInstruction.h"

@implementation SBCashStandingInstruction

- (id) initWithName: (NSString *) name andValue: (NSString *)value
{
    self = [super init];
    if (self) {
        self.name = name;
        self.value = value;
    }
    return self;
}

@end
