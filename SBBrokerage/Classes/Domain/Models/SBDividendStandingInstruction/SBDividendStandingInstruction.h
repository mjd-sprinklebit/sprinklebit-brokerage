//
//  SBDividendStandingInstruction.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 13/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBDividendStandingInstruction : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;

- (id) initWithName: (NSString *) name andValue: (NSString *)value;

@end
