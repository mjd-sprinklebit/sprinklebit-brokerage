//
//  SBTransaction.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 13/08/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBTransaction : NSObject

@property (nonatomic, strong) NSString *side;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSNumber *accountNumber;
@property (nonatomic, strong) NSNumber *amount;

@end
