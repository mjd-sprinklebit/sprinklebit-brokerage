//
//  SBBrokerageDataSource.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 30/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBBalance.h"

@interface SBBrokerageDataSource : NSObject

+ (NSArray*)getDocsList;
+ (NSDictionary*)getAccountTypeList;
+ (NSDictionary*)getAccountTradeTypeList;
+ (NSDictionary*)getCashStandingInstructionsList;
+ (NSDictionary*)getDividendStandingInstructionsList;
+ (NSDictionary*)getDividendReinsvestmentInstructionsList;
+ (NSDictionary*)getBrokerageAccountList;
+ (SBBalance*)getBalanceData;
+ (NSArray*)getTransactionsData;
+ (NSArray *)brokerageFees;

@end
