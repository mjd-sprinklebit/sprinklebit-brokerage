//
//  SBBrokerageDataSource.m
//  SBBrokerage
//
//  Created by Fernando Oliveira on 30/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import "SBBrokerageDataSource.h"
#import "SBTransaction.h"
#import "SBBalanceItem.h"
#import "SBBalance.h"
#import "SBAccountType.h"
#import "SBAccountTradeType.h"
#import "SBBrokerageAccount.h"
#import "SBCashStandingInstruction.h"
#import "SBDividendReinvestmentInstruction.h"
#import "SBDividendStandingInstruction.h"
#import "SBBrokerageFee.h"

@implementation SBBrokerageDataSource

+ (NSArray *) getDocsList {
    return [[NSArray alloc] initWithObjects:
            @"Business Continuity Plan Disclosure",
            @"Customer Identification Program Notice",
            @"Day Trading Risk Disclosure",
            @"Margin Disclosure Statement",
            @"Mutual Funds Breakpoint Discounts",
            @"Penny Stock Risk Disclosure",
            @"SEC Rule 606",
            nil];
}

+ (NSDictionary*) getAccountTypeList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Type 1", @"type1",
            @"Type 2", @"type2",
            @"Type 3", @"type3",
            @"Type 4", @"type4",
            @"Type 5", @"type5",
            @"Type 6", @"type6",
            @"Type 7", @"type7",
            @"Type 8", @"type8",
            @"Type 9", @"type9", nil];
}

+ (NSDictionary*) getAccountTradeTypeList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Trade Type 1", @"type1",
            @"Trade Type 2", @"type2",
            @"Trade Type 3", @"type3",
            @"Trade Type 4", @"type4",
            @"Trade Type 5", @"type5",
            @"Trade Type 6", @"type6",
            @"Trade Type 7", @"type7",
            @"Trade Type 8", @"type8",
            @"Trade Type 9", @"type9", nil];
}

+ (NSDictionary*) getCashStandingInstructionsList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Cash Standing 1", @"cashStanding1",
            @"Cash Standing 2", @"cashStanding2",
            @"Cash Standing 3", @"cashStanding3",
            @"Cash Standing 4", @"cashStanding4",
            @"Cash Standing 5", @"cashStanding5",
            @"Cash Standing 6", @"cashStanding6",
            @"Cash Standing 7", @"cashStanding7",
            @"Cash Standing 8", @"cashStanding8",
            @"Cash Standing 9", @"cashStanding9", nil];
}


+ (NSDictionary*) getDividendStandingInstructionsList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Dividend Standing 1", @"dividendStanding1",
            @"Dividend Standing 2", @"dividendStanding2",
            @"Dividend Standing 3", @"dividendStanding3",
            @"Dividend Standing 4", @"dividendStanding4",
            @"Dividend Standing 5", @"dividendStanding5",
            @"Dividend Standing 6", @"dividendStanding6",
            @"Dividend Standing 7", @"dividendStanding7",
            @"Dividend Standing 8", @"dividendStanding8",
            @"Dividend Standing 9", @"dividendStanding9", nil];
}

+ (NSDictionary*) getDividendReinsvestmentInstructionsList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Dividend Reinvestment 1", @"dividendReinvestment1",
            @"Dividend Reinvestment 2", @"dividendReinvestment2",
            @"Dividend Reinvestment 3", @"dividendReinvestment3",
            @"Dividend Reinvestment 4", @"dividendReinvestment4",
            @"Dividend Reinvestment 5", @"dividendReinvestment5",
            @"Dividend Reinvestment 6", @"dividendReinvestment6",
            @"Dividend Reinvestment 7", @"dividendReinvestment7",
            @"Dividend Reinvestment 8", @"dividendReinvestment8",
            @"Dividend Reinvestment 9", @"dividendReinvestment9", nil];
}

+ (NSDictionary*) getBrokerageAccountList {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Brokerage Account 1", @"brokerageAccount1",
            @"Brokerage Account 2", @"brokerageAccount2",
            @"Brokerage Account 3", @"brokerageAccount3",
            @"Brokerage Account 4", @"brokerageAccount4",
            @"Brokerage Account 5", @"brokerageAccount5",
            @"Brokerage Account 6", @"brokerageAccount6",
            @"Brokerage Account 7", @"brokerageAccount7",
            @"Brokerage Account 8", @"brokerageAccount8",
            @"Brokerage Account 9", @"brokerageAccount9", nil];
}

+ (SBBalance *) getBalanceData {
    SBBalanceItem *balanceItem1 = [SBBalanceItem new];
    balanceItem1.title = @"Total Account Equity";
    balanceItem1.startOfDayAmount = [NSNumber numberWithDouble:12534232.00];
    balanceItem1.realTimeAmount = [NSNumber numberWithDouble:12534232.00];
    balanceItem1.diffAmount = [NSNumber numberWithDouble:-232.00];
    
    SBBalanceItem *balanceItem2 = [SBBalanceItem new];
    balanceItem2.title = @"Total Market Value";
    balanceItem2.startOfDayAmount = [NSNumber numberWithDouble:134232.00];
    balanceItem2.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem2.diffAmount = [NSNumber numberWithDouble:4232.00];
    
    SBBalanceItem *balanceItem3 = [SBBalanceItem new];
    balanceItem3.title = @"Cash Available for Investment";
    balanceItem3.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem3.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem3.diffAmount = [NSNumber numberWithDouble:232.00];
    
    SBBalanceItem *balanceItem4 = [SBBalanceItem new];
    balanceItem4.title = @"Cash Available for Withdrawal";
    balanceItem4.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem4.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem4.diffAmount = [NSNumber numberWithDouble:32.00];
    
    SBBalanceItem *balanceItem5 = [SBBalanceItem new];
    balanceItem5.title = @"Buying Power";
    balanceItem5.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem5.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem5.diffAmount = [NSNumber numberWithDouble:32.00];
    
    SBBalanceItem *balanceItem6 = [SBBalanceItem new];
    balanceItem6.title = @"Securities";
    balanceItem6.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem6.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem6.diffAmount = [NSNumber numberWithDouble:32.00];
    
    SBBalanceItem *balanceItem7 = [SBBalanceItem new];
    balanceItem7.title = @"Long Stocks";
    balanceItem7.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem7.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem7.diffAmount = [NSNumber numberWithDouble:32.00];
    
    SBBalanceItem *balanceItem8 = [SBBalanceItem new];
    balanceItem8.title = @"Short Stocks";
    balanceItem8.startOfDayAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem8.realTimeAmount = [NSNumber numberWithDouble:34232.00];
    balanceItem8.diffAmount = [NSNumber numberWithDouble:32.00];
    
    SBBalance *balance = [SBBalance new];
    balance.items = [NSArray arrayWithObjects:balanceItem1, balanceItem2, balanceItem3, balanceItem4, balanceItem5, balanceItem6, balanceItem7, balanceItem8,nil];
    
    return balance;
}

+ (NSArray*) getTransactionsData {
    SBTransaction *transaction = [SBTransaction new];
    transaction.side = @"right";
    transaction.date = [NSDate date];
    transaction.accountName = @"Jonas Account";
    transaction.accountNumber = [NSNumber numberWithLong: 1928734981];
    transaction.amount = [NSNumber numberWithDouble:12534232.00];

    return [NSArray arrayWithObjects:
            transaction,
            transaction,
            transaction,
            transaction,
            transaction,
            transaction,
            transaction,
            transaction,nil];
}

+ (NSArray *)brokerageFees
{
    SBBrokerageFee *f1 = [SBBrokerageFee new];
    f1.ico = [UIImage imageNamed:@"ico-refresh-brokerage"];
    f1.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f1.feeValue = 12.1;
    f1.brokerageValue = 1;
    f1.color = [UIColor colorWithRed:0.282 green:0.482 blue:0.075 alpha:1];
    f1.percent = 50;
    f1.name = @"Brokerage 1";
    
    SBBrokerageFee *f2 = [SBBrokerageFee new];
    f2.ico = [UIImage imageNamed:@"ico-satellite"];
    f2.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f2.feeValue = 12.1;
    f2.brokerageValue = 1;
    f2.color = [UIColor colorWithRed:0.800 green:0.157 blue:0.153 alpha:1];
    f2.percent = 60;
    f2.name = @"Brokerage 2";
    
    SBBrokerageFee *f3 = [SBBrokerageFee new];
    f3.ico = [UIImage imageNamed:@"ico-tea"];
    f3.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f3.feeValue = 12.1;
    f3.brokerageValue = 1;
    f3.color = [UIColor colorWithRed:0.024 green:0.529 blue:0.890 alpha:1];
    f3.percent = 70;
    f3.name = @"Brokerage 3";
    
    SBBrokerageFee *f4 = [SBBrokerageFee new];
    f4.ico = [UIImage imageNamed:@"ico-light-bulb"];
    f4.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f4.feeValue = 12.1;
    f4.brokerageValue = 1;
    f4.color = [UIColor colorWithRed:0.008 green:0.659 blue:0.114 alpha:1];
    f4.percent = 10;
    f4.name = @"Brokerage 4";
    
    SBBrokerageFee *f5 = [SBBrokerageFee new];
    f5.ico = [UIImage imageNamed:@"ico-clipboard"];
    f5.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f5.feeValue = 12.1;
    f5.brokerageValue = 1;
    f5.color = [UIColor colorWithRed:0.549 green:0.267 blue:0.443 alpha:1];
    f5.percent = 20;
    f5.name = @"Brokerage 5";
    
    SBBrokerageFee *f6 = [SBBrokerageFee new];
    f6.ico = [UIImage imageNamed:@"ico-bag"];
    f6.desc = @"How much it costs for us to share the story of SprinkleBit with you and the world.";
    f6.feeValue = 12.1;
    f6.brokerageValue = 1;
    f6.color = [UIColor colorWithRed:0.008 green:0.565 blue:0.494 alpha:1];
    f6.percent = 90;
    f6.name = @"Brokerage 6";
    
    return @[f1,f2,f3,f4,f5,f6];
}



@end
