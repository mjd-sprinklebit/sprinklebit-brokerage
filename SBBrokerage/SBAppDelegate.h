//
//  SBAppDelegate.h
//  SBBrokerage
//
//  Created by Fernando Oliveira on 27/07/14.
//  Copyright (c) 2014 sprinklebit.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
